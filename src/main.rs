mod schema;
mod user;

use std::env;

use askama::Template;
use axum::{
    extract::Path, http::{header, HeaderMap, StatusCode}, response::{Html, IntoResponse}, routing::{get, post}, Json, Router
};
use deadpool_diesel::postgres::{Manager, Pool};
use dotenvy::dotenv;
use tokio::net::TcpListener;

#[derive(Clone)]
pub struct AppState {
    pool: Pool,
}

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate<'a> {
    name: &'a str,
}

#[tokio::main]
async fn main() {
    let pool = get_db_connection_pool();

    let app = Router::new()
        .route("/", get(main_handler))
        .route("/_assets/*path", get(handle_assets))
        .route("/users", post(user::handlers::create_user))
        .route("/users", get(user::handlers::list_users))
        .with_state(AppState { pool });

    let listener = TcpListener::bind("0.0.0.0:3000").await.unwrap();

    axum::serve(listener, app).await.unwrap();
}

async fn main_handler() -> Html<String> {
    let index = IndexTemplate { name: "world" };
    let html = index.render().unwrap();
    Html(html)
}

static THEME_CSS: &str = include_str!("../assets/css/styles.css");

async fn handle_assets(Path(path): Path<String>) -> impl IntoResponse {
    let mut headers = HeaderMap::new();

    println!("path: {}", path);

    if path == "css/styles.css" {
        headers.insert(header::CONTENT_TYPE, "text/css".parse().unwrap());
        return (StatusCode::OK, headers, THEME_CSS);
    }
    return (StatusCode::NOT_FOUND, headers, "");
}

fn get_db_connection_pool() -> Pool {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABSE_URL must be specefied");

    let manager = Manager::new(database_url, deadpool_diesel::Runtime::Tokio1);
    let pool = Pool::builder(manager).max_size(50).build().unwrap();

    return pool;
}
