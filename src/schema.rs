// @generated automatically by Diesel CLI.

diesel::table! {
    emails (id) {
        id -> Int4,
        email -> Varchar,
        user_id -> Int4,
    }
}

diesel::table! {
    users (id) {
        id -> Int4,
        #[max_length = 255]
        name -> Varchar,
        age -> Int4,
    }
}

diesel::joinable!(emails -> users (user_id));

diesel::allow_tables_to_appear_in_same_query!(
    emails,
    users,
);
