use axum::{extract::State, Json};

use crate::AppState;

use super::{
    model::{CreateUser, UserDTO},
    repository,
};

pub async fn create_user(
    State(state): State<AppState>,
    Json(user): Json<CreateUser>,
) -> Json<String> {
    repository::save(&state.pool, user).await.unwrap();
    return Json("User created".to_string());
}

pub async fn list_users(State(state): State<AppState>) -> Json<Vec<UserDTO>> {
    let users_all = repository::find_all(&state.pool)
        .await
        .expect("Error loading users");

    return Json(users_all);
}
