use crate::schema::{emails, users};

use diesel::prelude::*;
use diesel::{deserialize::Queryable, Selectable};
use serde::{Deserialize, Serialize};

// Database models

#[derive(Deserialize, Insertable)]
#[diesel(table_name = users)]
pub struct CreateUserDB {
    pub name: String,
    pub age: i32,
}

#[derive(Deserialize, Insertable)]
#[diesel(table_name = emails)]
pub struct CreateEmailDB {
    pub email: String,
    pub user_id: i32,
}

#[derive(Serialize, Queryable, Selectable, Identifiable, PartialEq)]
#[diesel(table_name = users)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct User {
    pub id: i32,
    pub name: String,
    pub age: i32,
}

#[derive(Serialize, Queryable, Selectable, Associations, Identifiable, PartialEq)]
#[diesel(belongs_to(User))]
#[diesel(table_name = emails)]
pub struct Email {
    pub id: i32,
    pub email: String,
    pub user_id: i32,
}

// DTOs

#[derive(Serialize)]
pub struct UserDTO {
    pub id: i32,
    pub name: String,
    pub age: i32,
    pub emails: Vec<EmailDTO>,
}

#[derive(Serialize)]
pub struct EmailDTO {
    pub id: i32,
    pub email: String,
}

#[derive(Deserialize)]
pub struct CreateUser {
    pub name: String,
    pub age: i32,
    pub emails: Vec<String>,
}
