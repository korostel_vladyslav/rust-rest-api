use std::error::Error;

use deadpool_diesel::postgres::Pool;
use diesel::prelude::*;
use diesel::{QueryDsl, SelectableHelper};

use crate::user::model::{CreateEmailDB, CreateUserDB};

use super::model::{CreateUser, Email, EmailDTO, User, UserDTO};

pub async fn save(pool: &Pool, user: CreateUser) -> Result<(), Box<dyn Error>> {
    use crate::schema::users::dsl::*;

    let connection = pool.get().await?;

    let create_user_db = CreateUserDB {
        name: user.name,
        age: user.age,
    };

    // Insert user into database
    let created_user: User = connection
        .interact(|conn| {
            diesel::insert_into(users)
                .values(create_user_db)
                .returning(User::as_returning())
                .get_result(conn)
                .expect("Error with insert user")
        })
        .await?;

    // Insert emails into database for the user
    for email in user.emails {
        let create_email_db = CreateEmailDB {
            email,
            user_id: created_user.id,
        };

        connection
            .interact(|conn| {
                diesel::insert_into(crate::schema::emails::table)
                    .values(create_email_db)
                    .execute(conn)
                    .expect("Error with insert email")
            })
            .await
            .expect("Error with insert email");
    }

    Ok(())
}

pub async fn find_all(pool: &Pool) -> Result<Vec<UserDTO>, Box<dyn Error>> {
    use crate::schema::users::dsl::*;

    let connection = pool.get().await?;

    let result = connection
        .interact(|conn| {
            // SELECT * FROM users
            let all_users: Vec<User> = users
                .select(User::as_select())
                .load(conn)
                .expect("Error loading posts");

            // SELECT * FROM emails WHERE user_id IN (1, 2, 3, ...)
            let user_emails: Vec<Email> = Email::belonging_to(&all_users)
                .select(Email::as_select())
                .load(conn)
                .expect("Error loading emails");

            // Group emails by user using grouped_by method (It's not a GROUP BY clause in SQL)
            let users_with_emails = user_emails
                .grouped_by(&all_users)
                .into_iter()
                .zip(all_users)
                .map(|(emails, user)| UserDTO {
                    id: user.id,
                    name: user.name,
                    age: user.age,
                    emails: emails
                        .into_iter()
                        .map(|email| EmailDTO {
                            id: email.id,
                            email: email.email,
                        })
                        .collect(),
                })
                .collect();

            return users_with_emails;
        })
        .await?;

    Ok(result)
}
